import xs, {Stream, Listener} from 'xstream';
import fetch, { RequestInit } from 'node-fetch';

export interface Request {
    url: string;
    options?: RequestInit

    context: {
        category: string;
        data?: any;
    };
}

export interface Response {
    body: string;
    context: {
        category: string;
        data?: any;
    };
}

export type HTTPSource = Stream<Response>;
export type HTTPSink = Stream<Request>;

export const makeHTTPDriver = () => (source: Stream<Request>) => {
    let emit: (r: Response) => void = () => {};
    let emitError: (error: string) => void = () => {};

    source.addListener({
        next: (r) => {
            fetch(r.url, r.options)
                .then(r => r.text())
                .then(t => emit({ body: t, context: r.context }))
                .catch(r => emitError(r));
        }
    })

    return xs.create({
        start(listener: Listener<Response>) {
            emit = (r: Response) => listener.next(r);
            emitError = (error: string) => listener.error(error);
        },
        stop() {
            emit = () => {};
            emitError = () => {};
        }
    })
}