import xs, {Stream, Listener} from 'xstream';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import fetch from 'node-fetch';

interface User {
    id: number;
    first_name: string;
    last_name?: string;
    username?: string;
    language_code?: string;
}

interface Chat {
    id: number;
    type: "private" | "group" | "supergroup" | "channel";
    title?: string;
    username?: string;
    first_name?: string;
    last_name?: string;
    all_members_are_administrators: boolean;
}

interface MessageEntity {
    type: "mention" | "hashtag" | "bot_command" | "url" | "email" | "bold" | "italic" | "code" | "pre" | "text_link" | "text_mention";
    offset: number;
    length: number;
    url?: string;
    user?: User;
}

interface Message {
    message_id: number;
    from: User;
    date: number;
    chat: Chat;
    text?: string;
    reply_to_message?: Message;
    entities?: MessageEntity[];
    forward_from?: User;
    forward_from_chat: Chat;
    forward_from_message_id: number;
    forward_id: number;
}

export interface IncomingTelegramMessage {
    update_id: number;
    message?: Message;
    edited_message?: Message;
    channel_post?: Message;
    edited_channel_post?: Message;
}
export interface OutgoingTelegramMessage {
    chat: string | number;
    text: string;
    reply_to?: number;
}

interface TelegramConfig {
    botToken: string;
    method: "poll" | "webhook";
    webhookURL?: string;
    webhookPort?: number;
    pollInterval?: number;
    token?: string;
}

function parseTelegramMessage(obj: any): Promise<IncomingTelegramMessage> {
    return new Promise((resolve, reject) => {
        resolve(obj as IncomingTelegramMessage)
    }) 
}

async function doTelegramCall(method: string, payload: any, config: TelegramConfig): Promise<any> {
    let response = await fetch(`https://api.telegram.org/bot${config.botToken}/${method}`, {
        method: 'POST',
        headers: {
            'Content-Type': "application/json"
        },
        body: JSON.stringify(payload)
    });
    return await response.json();
}

function sendTelegramMessage(msg: OutgoingTelegramMessage, config: TelegramConfig): void {
    doTelegramCall('sendMessage', {
        chat_id: msg.chat,
        text: msg.text,
        reply_to_message_id: msg.reply_to
    }, config)
}

export type TelegramSource = Stream<IncomingTelegramMessage>;
export type TelegramSink = Stream<OutgoingTelegramMessage>;

export const TelegramDriver = (config: TelegramConfig) => {
    return (sink: Stream<OutgoingTelegramMessage>) => {
        
        // handle outgoing messages
        sink.addListener({
            next(m: OutgoingTelegramMessage) {
                sendTelegramMessage(m, config)
            }
        })

        // handle incoming messages
        let emit: ((m: IncomingTelegramMessage) => void) = () => {};

        switch(config.method) {
            case 'webhook':
                doTelegramCall('setWebhook', { url: `${config.webhookURL!}/${config.token!}/` }, config);

                let app = express();

                app.use(bodyParser.json());

                app.post(`/${config.token}`, (req, res) => {
                    parseTelegramMessage(req.body)
                        .then(parsed => emit(parsed));
                    res.status(200).send('ok');
                });
                app.listen(config.webhookPort!);
                console.log("Listening on port " + config.webhookPort);
                break;

            case 'poll':
                let offset: null | number = null;
                xs.periodic(config.pollInterval || 1000)
                    .addListener({
                        next() {
                            doTelegramCall('getUpdates', offset ? {offset} : {}, config)
                                .then(r => {
                                    const max_id = Math.max(...r.result.map((e: any) => e.update_id)) + 1;
                                    offset = max_id > -1 ? max_id : offset;
                                    return r 
                                })
                                .then(r => r.result as any[])
                                .then(updates => Promise.all(updates.map(u => parseTelegramMessage(u))))
                                .then(messages => messages.forEach(m => emit(m)))

                        }
                    })
                break;
        }

        return xs.createWithMemory({
            start: (listener: Listener<IncomingTelegramMessage>) => {
                emit = (m) => listener.next(m);
            },
            stop: () => {
                emit = () => {};
            }
        })
    }
}
