import xs, { Stream } from 'xstream';
import {run} from '@cycle/run';
import {TelegramDriver, TelegramSink, TelegramSource, OutgoingTelegramMessage } from './drivers/telegram';
import { makeHTTPDriver, HTTPSource, HTTPSink } from './drivers/http';
import { ConversionPlugin } from './plugins/conversion';
import { NSFWWarning } from './plugins/nsfw-warning';
import { ChatCommands } from './plugins/commands';
import { DiceRolls } from './plugins/dice';
import { EightBall } from './plugins/8ball';

import {get} from 'lodash';

export interface Sources {
    telegram: TelegramSource;
    http: HTTPSource
}

export interface Sinks {
    telegram: TelegramSink;
    http: HTTPSink
}

const pluginList: ((s: Sources) => Partial<Sinks>)[] = [
    ConversionPlugin,
    NSFWWarning,
    ChatCommands,
    DiceRolls,
    EightBall
]

run((sources: Sources): Sinks => {
    let plugins = pluginList.map(f => f(sources));
    return {
        telegram: xs.merge(...plugins.filter(p => "telegram" in p).map(p => p.telegram!)),
        http: xs.merge(...plugins.filter(p => "http" in p).map(p => p.http!))
    }
}, {
    telegram: TelegramDriver(process.env['WEBHOOK_PORT'] ? {
        botToken: process.env['BOT_TOKEN'], 
        method: "webhook",
        webhookURL: process.env['WEBHOOK_URL'],
        webhookPort: process.env['WEBHOOK_PORT'],
        token: process.env['SECRET_TOKEN']
    } :
    // dev environment
    {
        botToken: process.env['BOT_TOKEN'],
        method: "poll",
        pollInterval: 1000
    }),
    http: makeHTTPDriver()
})
