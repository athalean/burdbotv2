import {Sources, Sinks} from '../main';
import xs from 'xstream';

const answers = [
    "It is certain",
    "It is decidedly so",
    "Without a doubt",
    "Yes definitely",
    "You may rely on it",
    "As I see it, yes",
    "Most likely",
    "Outlook good",
    "Yes",
    "Signs point to yes",
    "Reply hazy, try again",
    "Ask again later",
    "Better not tell you now",
    "Cannot predict now",
    "Concentrate and ask again",
    "Don't count on it",
    "My reply is no",
    "My sources say no",
    "Outlook not good",
    "Very doubtful"
];

function choice<T>(x: T[]): T {
    let length = x.length;
    let index = Math.floor(Math.random()*length);
    return x[index];
}

export const EightBall = ({telegram}: Sources): Partial<Sinks> => {
    let eightBallCommands = telegram
        .filter(e => !!(e.message && e.message!.text))
        .filter(e => e.message!.text!.startsWith("/8ball"))
        .map(e => {
            return {
                chat: e.message!.chat!.id,
                user: e.message!.from.first_name,
                replyTo: e.message!.message_id
            }
        });
    return {
        telegram: eightBallCommands
            .map(({chat, user, replyTo}) => {
                return {
                    chat,
                    reply_to: replyTo,
                    text: choice(answers)
                };
            })
    }
}