import {Sources, Sinks} from '../main';
import xs from 'xstream';

interface CommandList {
    [key: string]: (arg: string) => string
};

let commandList: CommandList = {
        "/preen": (argument: string) => `*preens ${argument} thoroughly*`,
        "/cookie": (argument: string) => `*tosses a cookie to ${argument}*`,
        "/coffee": (argument: string) => `*opens ${argument}'s beak for a hot cup of coffee!*`,
        "/tea": (argument: string) => `*opens ${argument}'s beak for a hot cup of tea!*`,
        "/pun": (argument: string) => `*plays a rimshot for ${argument}'s horrible pun and shakes the pun jar*`,
        "/breakfast": (argument: string) => `*feeds ${argument} a sandwich, a cookie and a choice of tea or coffee*`,
        "/fish": (argument: string) => `*tosses a fish to ${argument}*`,
        "/hug": (argument: string) => `*hugs ${argument} tight* ^v^`,
        "/peck": (argument: string) => `*pecks at ${argument} for attention*`,
        "/floof": (argument: string) => `*floofs up ${argument}*`,
        "/energydrink": (argument: string) => `*opens ${argument}'s beak for an ice cold energy drink*`,
        "/cocoa": (argument: string) => `*opens ${argument}'s beak for a cup of hot chocolate*`,
        "/help": () => {
            return `Hi, I'm Burdbot! I'm here to entertain you all and sometimes to do something useful. ^v^\n\nI can do the following commands and features:\n\n${
                Object.keys(commandList).join("\n")
            }`
        }
};

export const ChatCommands = ({telegram}: Sources) => {
    let commandsRegex = new RegExp(`(${Object.keys(commandList).map(e => `(${e.replace('/', '\\/')})`).join("|")})`);
    return {
        telegram: telegram
                    .filter(m => !!(m.message && m.message.text && commandsRegex.test(m.message.text)))
                    .map(m => { return {
                        user: m.message!.from.first_name || m.message!.from.username,
                        chat: m.message!.chat.id
                    }})
                    .map(({user, chat}) => 
                        telegram
                            .filter(m => !!(m.message && m.message!.text))
                            .map(m => xs.fromArray(
                                Object.keys(commandList).map(command => 
                                        m.message!.text!.match(new RegExp(command.replace('/', '\\/') + "(?:(\\s+.*?)(?:\\s|$|[!:\.,;?]))?", 'g'))
                                    )
                                )
                            ).flatten()
                            .filter(e => !!(e && e.length > 0))
                            .map(e => xs.fromArray(e!)).flatten()
                            .map(query => {
                                let [_, command, argument] = /(.*?)(?:\s(.*)|$)/.exec(query)!;
                                return {
                                    command: command.trim(), argument: argument && argument.trim()
                                } 
                            })
                            .map(({command, argument}) => {
                                return {
                                    command: command,
                                    argument: argument,
                                    user: user,
                                    chat: chat
                                }
                            })
                    ).flatten()
                    .filter(({command}) => Object.keys(commandList).indexOf(command) > -1)
                    .map(({command, argument, user, chat}) => {
                        return {
                            chat: chat,
                            text: commandList[command](argument || user!)
                        }
                    })
    }
};
