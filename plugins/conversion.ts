import {Sources, Sinks} from '../main'
import xs, { Stream } from 'xstream';
import {OutgoingTelegramMessage} from '../drivers/telegram';
import { get } from 'lodash';

interface Unit {
    value: number,
    unit: string
}

function identifyUnits(source: Stream<string>): Stream<Unit[]> {
    const units = [
        {
            type: "meter",
            regex: /(\d+(?:\.\d+)?) *?(?:m|[Mm]et(?:er|re)s?)(?:\s|$|[.,;?!])/
        },
        {
            type: "centimeter",
            regex: /(\d+(?:\.\d+)?) *?(?:cm|[Cc]entimet(?:er|re)s?)(?:\s|$|[.,;?!])/
        },
        {
            type: "kilometer",
            regex: /(\d+(?:\.\d+)?) *?(?:km|[Kk]ilomet(?:er|re)s?)(?:\s|$|[.,;?!])/
        },
        {
            type: "yard",
            regex: /(\d+(?:\.\d+)?) *?(?:[Yy]ards?|yd)(?:\s|$|[.,;?!])/
        },
        {
            type: "miles",
            regex: /(\d+(?:\.\d+)?) *?(?:[Mm]iles|mil)(?:\s|$|[.,;?!])/
        },
        {
            type: "feet",
            regex: /(\d+(?:\.\d+)?) *?(?:ft|f(?:oo|ee)t|\')(?:\s|$|[.,;?!])/
        },
        {
            type: "inches",
            regex: /(\d+(?:\.\d+)?) *?(?:in(?:ch(?:es)?)?|\'\'|\")(?:\s|$|[.,;?!])/
        },
        {
            type: "kilograms",
            regex: /(\d+(?:\.\d+)?) *?(?:kg|[Kk]ilogram(?:mes)?)(?:\s|$|[.,;?!])/
        },
        {
            type: "pound",
            regex: /(\d+(?:\.\d+)?) *?(?:lb|[Pp]ounds?)(?:\s|$|[.,;?!])/
        },
        {
            type: "kmh",
            regex: /(\d+(?:\.\d+)?) *?(?:km\/?h|kph)(?:\s|$|[.,;?!])/
        },
        {
            type: "mph",
            regex: /(\d+(?:\.\d+)?) *?(?:mph|mi\/?h)(?:\s|$|[.,;?!])/
        },
        {
            type: "ounces",
            regex: /(\d+(?:\.\d+)?) *?(?:oz|[Oo]unces?)(?:\s|$|[.,;?!])/
        },
        {
            type: "degree_c",
            regex: /(\d+(?:\.\d+)?) *?(?:°[Cc](?:elsius)?)(?:\s|$|[.,;?!])/
        },
        {
            type: "degree_f",
            regex: /(\d+(?:\.\d+)?) *?(?:°[Ff](?:ahrenheit)?)(?:\s|$|[.,;?!])/
        },

        // easteregg
        {
            type: "goodiness",
            regex: /(?:than|at least) (\d+(?:\.\d+)?) (?:times as good|as much better)/
        },
    ];
    const hasUnits = new RegExp("(?:"+ units.map(u => `(${u.regex.source})`).join("|") + ")");
    return source.filter(e => hasUnits.test(e)).map(e =>
        units.map(r => {
            let groups = r.regex.exec(e);
            return groups && {unit: r.type, value: parseFloat(groups[1])}
        })
        .filter(e => e !== undefined && e != null).map(e => e!)
    )
}

interface UnitConversion {
    conversions: Unit[],
    type: "us-to-metric" | "metric-to-us" | "mixed"
}

function convertUnits(source: Stream<Unit[]>): Stream<UnitConversion> {
    return source.map(units => {
        return {
            conversions: units.map(u => {
                switch(u.unit) {
                    case "meter":
                        return {
                            value: 3.2804 * u.value,
                            unit: "feet"
                        }
                    case "kilometer":
                        return {
                            value: 0.621371 * u.value,
                            unit: "miles"
                        }
                    case "centimeter":
                        return {
                            value: 0.393701 * u.value,
                            unit: "inches"
                        }
                    case "yard":
                        return {
                            value: 0.9144 * u.value,
                            unit: "meter"
                        }
                    case "miles":
                        return {
                            value: 1.60934 * u.value,
                            unit: "kilometer"
                        }
                    case "feet":
                        return {
                            value: 0.3048 * u.value,
                            unit: "meter"
                        }
                    case "inches":
                        return {
                            value: 2.24 * u.value,
                            unit: "centimeter"
                        }
                    case "kilograms":
                        return {
                            value: 2.20462 * u.value,
                            unit: "pound"
                        }
                    case "pound":
                        return {
                            value: 0.453592 * u.value,
                            unit: "kilograms"
                        }
                    case "kmh":
                        return {
                            value: 0.62137 * u.value,
                            unit: "mph"
                        }
                    case "mph":
                        return {
                            value: 1.60934 * u.value,
                            unit: "kph"
                        }
                    case "ounces":
                        return {
                            value: 28.3495 * u.value,
                            unit: "gram"
                        }
                    case "degree_c":
                        return {
                            value: u.value * 1.8 + 32,
                            unit: "degree_f"
                        }
                    case "degree_f":
                        return {
                            value: (u.value - 32) / 1.8,
                            unit: "degree_c"
                        }

                    // easteregg
                    case "goodiness":
                        return {
                            value: (u.value ** 25.32) % 3927,
                            unit: "feathers"
                        }
                    default:
                        return null;
                }
            }).filter(e => e !== null && e !== undefined).map(e => e!),
            type: (() => {
                let count = units.reduce((acc, next) => {
                    if (["yard", "miles", "feet", "degree_f", "inches", "mph", "pound", "ounces"].indexOf(next.unit) > -1)
                        return {...acc, us: acc.us + 1}
                    else
                        return {...acc, metric: acc.metric + 1}
                }, {us: 0, metric: 0});
                
                if (count.us === 0)
                    return "metric-to-us" as "metric-to-us";
                if (count.metric === 0)
                    return "us-to-metric" as "us-to-metric";
                return "mixed" as "mixed";
            })()
        }
    })
}

function displayUnit(u: Unit): string {
    let num = u.value.toFixed(2);
     switch(u.unit) {
        case "meter":
            return `${num}m`
        case "kilometer":
            return `${num}km`
        case "centimeter":
            return `${num}cm`
        case "yard":
            return `${num} yards`
        case "miles":
            return `${num} miles`
        case "feet":
            return `${num} ft`
        case "inches":
            return `${num} inches`
        case "degree_c":
            return `${num}°C`
        case "degree_f":
            return `${num}°F`
        case "pound":
            return `${num}lb`
        case "kilograms":
            return `${num}kg`
        case "kmh":
            return `${num} km/h`
        case "mph":
            return `${num} mph`
        case "gram":
            return `${num}g`

        // easteregg
        case "feathers":
            return `a ${parseInt(num)}th the intensity of the feeling when you realize a gryphon loves you`
        default:
            return `${num} ${u.unit}`;
    }
}


export const ConversionPlugin = (sources: Sources): Partial<Sinks> => {
    return {
        telegram: sources.telegram
            .filter(e => get(e, 'message.chat') !== undefined)
            .map(e => { 
                return {
                    chat: e.message!.chat!.id,
                    reply_to: e.message!.message_id
                }
            })
            .map(({chat, reply_to}) =>
                sources.telegram
                    .filter(e => get(e, "message.text") !== undefined)
                    .map(e => e.message!.text)
                    .compose(identifyUnits)
                    .compose(convertUnits)
                    .map(e => {
                        return {
                            chat: chat,
                            text: `(that's ${
                                e.conversions.length > 1 
                                    ? e.conversions.splice(0, e.conversions.length - 1).map(u => displayUnit(u)).join(", ") + " and " + displayUnit(e.conversions[e.conversions.length - 1]) 
                                    : e.conversions.map(u => displayUnit(u)).join(", ")
                                } ${
                                    e.type === "us-to-metric" ? "for sane people" :
                                    e.type === "metric-to-us" ? "for confused people" :
                                    ". Also why do you mix these?"
                                })`,
                              reply_to: reply_to
                        }
                    })
            ).flatten()
    }
}
