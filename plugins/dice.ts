import {Sources, Sinks} from '../main';
import xs from 'xstream';

export const DiceRolls = ({telegram}: Sources): Partial<Sinks> => {
    const dieRX = /\/(\d+)d(\d+)(?:\+(\d+))?/;
    let rollCommands = telegram
        .filter(e => !!(e.message && e.message!.text))
        .map(e => {
            return {
                chat: e.message!.chat!.id,
                user: e.message!.from.first_name,
                replyTo: e.message!.message_id
            }
        })
        .map(({user, chat, replyTo}) => telegram
            .filter(e => !!(e.message && e.message.text && dieRX.test(e.message.text)))
            .map(e => dieRX.exec(e.message!.text!)!)
            .map(([_, sNum, sSize, sBonus]) => {
                let num = parseInt(sNum);
                let size = parseInt(sSize);
                let bonus = parseInt(sBonus || "0");
                return {user, chat, replyTo, roll: { num, size, bonus }};
            })
        ).flatten();
    return {
        telegram: xs.merge(
            rollCommands
                        .filter(({roll: {num}}) => num <= 50)
                        .map(({user, chat, replyTo, roll: {num, size, bonus}}) => {
                            return {
                                rolls: Array.from(new Array(num)).map(e => Math.round(Math.random() * size + 1)),
                                bonus: bonus,
                                user,
                                chat,
                                replyTo
                            }
                        })
                        .map(({rolls, bonus, user, chat, replyTo}) => {
                            let sum = rolls.reduce((a,n) => a + n, 0) + bonus;
                            return {
                                chat: chat,
                                reply_to: replyTo,
                                text: `${user} rolls ${sum} (${rolls.join("+")}).`
                            }
                        }),
                rollCommands
                    .filter(({roll: {num}}) => num > 50)
                    .map(({user, chat, replyTo, roll: {num}}) => {
                        return {
                            chat: chat,
                            reply_to: replyTo,
                            text: `${user} rolls themselves to death with ${num} dice.`
                        }
                    })
        )
    }
}