import {Sources, Sinks} from '../main';
import xs from 'xstream';

export const NSFWWarning = ({telegram, http}: Sources): Partial<Sinks> => {
    let urls = telegram
            .filter(e => !!(e.message && e.message.chat))
            .map(e => {
                return {
                    chat: e.message!.chat.id!,
                    replyTo: e.message!.message_id
                }
            })
            .map(({chat, replyTo}) =>
                telegram
                    .filter(e => !!(e.message!.text))
                    .filter(e => !!(e.message!.entities && e.message!.entities!.findIndex(e => e.type === "url") > -1))
                    .map(m => m.message!.entities!
                                .filter(e => e.type === "url")
                                .map(e => m.message!.text!.substr(e.offset, e.length) ))
                    .map(urls => {
                        return xs.fromArray(urls.map(url => { return { url, chat, replyTo }}))
                    }).flatten()
            ).flatten();
    let faLinks = urls.filter(({url}) => /https?:\/\/(www.)?furaffinity.net\/view\/.*?/.test(url));

    return {
        telegram: http
            .filter(e => e.context.category === 'fa-nsfw-check')
            .filter(({body}) => body.indexOf("due to the content filter settings") > -1)
            .map(({body, context}) => {
                return {
                    chat: context.data.chat,
                    reply_to: context.data.replyTo,
                    text: `↑❗️ NSFW Warning! This picture is tagged as "mature" or "adult" - please click at your own discretion!`
                }
            }),
        http: faLinks.map(({url, chat, replyTo}) => {
            return {
                url: url,
                context: {
                    category: 'fa-nsfw-check',
                    data: {
                        chat: chat,
                        replyTo: replyTo
                    }
                }
            }
        })
    }
}